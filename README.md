# Wingo WM

### Installation

Make sure you have the [go language](https://golang.org/) installed, then
install the `wingo` package.

```bash
go get gitlab.com/yonderbread/wingo
```

Command to start wingo.
(Put this in a startup script!)

```bash
exec wingo
```


To replace the currently running window manager with wingo:

```bash
wingo --replace
```

### Configuration

Generate a new config in `$HOME/.config/wingo`

```bash
wingo --write-config
```

Add workspace

```bash
AddWorkspace "workspace1"
```

Add a dynamically-named workspace

```bash
AddWorkspace (Input "Enter workspace name:")
```
